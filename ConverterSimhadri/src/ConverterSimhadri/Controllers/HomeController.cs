﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using ConverterSimhadri.Models;


namespace ConverterSimhadri.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            ViewData["Title"] = "Converter App by Simhadri";
            ViewData["Result"] = "";
            ViewData["Value"] = "";

            Converter converter = new Converter();
            return View(converter);
        }

        public IActionResult Convert(Converter converter)
        {
            if (ModelState.IsValid)
            {
                int temp = (int)((converter.Temperature_F - 32) * 5.0 / 9.0);
                ViewData["Title"] = "Converted by Simhadri";
                ViewData["Result"] = "Temperature in C = " + temp;
                ViewData["Value"] = temp;
            }
            return View("Index", converter);
        }

    }
}
